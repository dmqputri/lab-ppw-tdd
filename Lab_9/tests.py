from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import books, home, search, tambah, kurang, login, logout

# Create your tests here.


class Lab9UnitTest(TestCase):
	def test_lab_9_list_book_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_using_books_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story9.html')

	def test_lab11_using_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_lab_11_using_tambah(self):
		found = resolve('/add')
		self.assertEqual(found.func, tambah)

	def test_lab_11_using_kurang(self):
		found = resolve('/remove')
		self.assertEqual(found.func, kurang)

	def test_lab_9_login_is_exist(self):
		response = Client().get('/login')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_file_is_exist(self):
		response = Client().get('/jsonfile')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_search_is_exist(self):	
		response = Client().get('/jsonsearch?cari=')
		self.assertEqual(response.status_code, 200)

	def test_landing_page(self):
		response = Client().get('/')
		html_response = response.content.decode('utf-8')
		self.assertIn('List of Books', html_response)
	
	def test_lab_11_using_logins(self):
		found = resolve('/login')
		self.assertEqual(found.func, login)